#!/usr/bin/python3

import _thread
import time
from bluetooth import *
import serial



def bt2ser():
    try:
         count=0
         while True:
            data = client_sock.recv(384) #Recieves 384 bytes of data.
            for i in data:
               if i=="\r\n":
                  break
               else:
                  count=count+1

            if count == 383:
               port1.write(data)
               continue
            else:
               print("Data not equal to 384 bytes")
               """s = input("Do you want to continue? y/n")
               if s =="y":
                  continue
               elif s=="n":
                  break
               else:
                  raise IOError"""


            #print(data)
            #port1.write(data)
    except IOError:
        pass

    

def ser2bt():
    try:
        while True:
            count=0
            serial_line = port1.readline() #Recieves a line of data that ends with CRLF character.
            for i in serial_line:
               if i=="\r\n":
                  break
               else:
                  count=count+1

            if count == 383:
               port1.write(data)
               continue
            else:
               print("Data not equal to 384 bytes")
            server_sock.send(serial_line)
    except IOError:
        pass


server_sock=BluetoothSocket( RFCOMM )
server_sock.bind(("",3))
server_sock.listen(1)

port = server_sock.getsockname()[1]

port1 = serial.Serial("/dev/ttyAMA0", baudrate=38400, timeout=3.0)#establishing serial connection
uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
advertise_service( server_sock, "SampleServer",
                   service_id = uuid,
                   service_classes = [ uuid, SERIAL_PORT_CLASS ],
                   profiles = [ SERIAL_PORT_PROFILE ],
#                   protocols = [ OBEX_UUID ]
                    )

print("Waiting for connection on RFCOMM channel %d" % port)


client_sock, client_info = server_sock.accept()
print("Accepted connection from ", client_info)
try:
   _thread.start_new_thread( bt2ser, (), )
   _thread.start_new_thread( ser2bt, (), )
except:
   print ("Error: unable to start thread")

while 1:
   pass

print("disconnected")
port1.close()
client_sock.close()
server_sock.close()
print("all done")