from bluetooth import *
import serial

server_sock=BluetoothSocket( RFCOMM )
server_sock.bind(("",1))
server_sock.listen(1)

port = server_sock.getsockname()[1]

uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

advertise_service( server_sock, "SampleServer",
                   service_id = uuid,
                   service_classes = [ uuid, SERIAL_PORT_CLASS ],
                   profiles = [ SERIAL_PORT_PROFILE ],
#                   protocols = [ OBEX_UUID ]
                    )

print("Waiting for connection on RFCOMM channel %d" % port)


s=bytes("Recieved:",encoding="utf-16")
client_sock, client_info = server_sock.accept()
print("Accepted connection from ", client_info)

try:
    while True:
        data = client_sock.recv(5) #Recieves 5 bytes of data.
        if len(data) == 0: break
        print(data)
        s = data+s
except IOError:
    pass

print(s)
port = serial.Serial("/dev/ttyAMA0", baudrate=38400, timeout=3.0)
port.write(s)


print("disconnected")

client_sock.close()
server_sock.close()
print("all done")
