#BT BRidge
#you need to set the BT in compatability mode first
#Disable serial console if enabled
#Enable Serial Port HW

from bluetooth import *
import time
import serial

print ("Starting program")

ser = serial.Serial('/dev/ttyS0', baudrate=38400,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS
                    )

server_sock=BluetoothSocket( RFCOMM )
server_sock.bind(("",PORT_ANY))
server_sock.listen(1)

port = server_sock.getsockname()[1]

uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
advertise_service( server_sock, "SampleServer",
                   service_id = uuid,
                   service_classes = [ uuid, SERIAL_PORT_CLASS ],
                   profiles = [ SERIAL_PORT_PROFILE ], 
#                   protocols = [ OBEX_UUID ] 
                    )
                   
print("Waiting for connection on RFCOMM channel %d" % port)

client_sock, client_info = server_sock.accept()
print("Accepted connection from ", client_info)

time.sleep(1)
try:
    #ser.write('Hello World\r\n')
    #ser.write('Serial Communication Using Raspberry Pi\r\n')
    #ser.write('By: Embedded Laboratory\r\n')
    print ("Data Echo Mode Enabled")
    while True:
        if ser.inWaiting() > 0:
            dataSER = ser.read()
    	    client_sock.send(dataSER)
            data = client_sock.recv(320)
	        ser.write(data)
       	    if len(data) == 0: break
        
except KeyboardInterrupt:
    print ("Exiting Program")
except IOError:
    pass
except:
    print ("Error Occurs, Exiting Program")

finally:
    ser.close()
    pass
    print("disconnected BT")

    client_sock.close()
    server_sock.close()
    print("all done")